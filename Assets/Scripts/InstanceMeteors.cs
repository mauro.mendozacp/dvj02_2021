﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanceMeteors : MonoBehaviour
{
    public GameObject meteorPrefab;
    public List<GameObject> generatedMeteors = new List<GameObject>();

    private float timer;
    public int meteorsForSeg;

    public Vector3 minRespawnPosition;
    public Vector3 maxRespawnPosition;

    private int meteorsCount;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0f;
        meteorsCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= meteorsForSeg)
        {
            timer = 0;

            GameObject met = Instantiate(meteorPrefab).gameObject;

            met.name = "Meteor " + (meteorsCount + 1);

            Vector3 auxPos = Vector3.zero;
            auxPos.x = Random.Range(minRespawnPosition.x, maxRespawnPosition.x);
            auxPos.y = minRespawnPosition.y;
            auxPos.z = Random.Range(minRespawnPosition.z, maxRespawnPosition.z);
            met.transform.position = auxPos;

            generatedMeteors.Add(met);

            met.transform.parent = transform;

            meteorsCount++;
        }
    }
}
