﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    private Light pLight;

    public float frequency;
    public float magnitude;
    public float minMagnitude;

    // Start is called before the first frame update
    void Start()
    {
        pLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        pLight.intensity = minMagnitude + (magnitude + Mathf.Sin(Time.timeSinceLevelLoad * frequency) * magnitude);
    }
}