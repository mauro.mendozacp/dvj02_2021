﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public enum Focus
    {
        Mercury,
        Venus,
        Earth,
        Mars,
        Jupiter,
        Saturn,
        Uranus,
        Neptune,
        Ship,
        Default
    }

    public Focus focus = Focus.Default;
    public GameObject cameraFocus;

    private Rigidbody rb;
    public float speed;
    public float maxSpeed;
    public GameObject reference;

    private Vector3 defaultPosition = new Vector3(115.0f, 2.5f, -65.0f);

    private GameObject instancing;

    void ChangeFocus()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            focus = Focus.Mercury;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            focus = Focus.Venus;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            focus = Focus.Earth;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            focus = Focus.Mars;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            focus = Focus.Jupiter;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            focus = Focus.Saturn;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            focus = Focus.Uranus;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            focus = Focus.Neptune;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            focus = Focus.Ship;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            focus = Focus.Default;
        }
    }

    void ChangePosition()
    {
        Vector3 v3 = Vector3.zero;
        float spacing = 0;

        switch (focus)
        {
            case Focus.Mercury:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[0].transform.position;
                spacing = 3.0f;
                break;
            case Focus.Venus:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[1].transform.position;
                spacing = 5.0f;
                break;
            case Focus.Earth:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[2].transform.position;
                spacing = 5.0f;
                break;
            case Focus.Mars:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[3].transform.position;
                spacing = 3.0f;
                break;
            case Focus.Jupiter:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[4].transform.position;
                spacing = 25.0f;
                break;
            case Focus.Saturn:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[5].transform.position;
                spacing = 23.0f;
                break;
            case Focus.Uranus:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[6].transform.position;
                spacing = 10.0f;
                break;
            case Focus.Neptune:
                v3 = instancing.GetComponent<InstancePlanets>().generatedPlanets[7].transform.position;
                spacing = 10.0f;
                break;
            case Focus.Ship:
                break;
            case Focus.Default:
                v3 = defaultPosition;
                cameraFocus.transform.eulerAngles = new Vector3(0, -45.0f, 0);
                break;
            default:
                break;
        }

        if (focus != Focus.Ship)
        {
            cameraFocus.transform.position = new Vector3(v3.x, v3.y, v3.z - spacing);
            cameraFocus.transform.LookAt(v3);
        }
    }

    void Move()
    {
        if (focus == Focus.Ship)
        {
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");

            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }

            rb.AddForce(ver * reference.transform.forward * speed);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Planet>())
        {
            float distance = Vector3.Distance(other.GetComponent<Planet>().transform.position, transform.position);
            float max = GetComponent<SphereCollider>().radius * transform.localScale.x + other.GetComponent<SphereCollider>().radius * other.transform.localScale.x;
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1, distance / max);

            print("Opacidad: " + distance / max);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Planet>())
        {
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        instancing = GameObject.FindWithTag("Solar System");
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Move();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeFocus();
    }

    private void LateUpdate()
    {
        ChangePosition();
    }
}
