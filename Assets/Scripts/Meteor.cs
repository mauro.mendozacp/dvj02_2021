﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    private GameObject instanceMet;

    public float maxPositionY;
    public float speedY;

    void Move()
    {
        Vector3 auxPos = transform.position;
        auxPos.y -= speedY * Time.deltaTime;
        transform.position = auxPos;
    }

    void CheckLimit()
    {
        if (transform.position.y <= maxPositionY)
        {
            instanceMet.GetComponent<InstanceMeteors>().generatedMeteors.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Planet>())
        {
            instanceMet.GetComponent<InstanceMeteors>().generatedMeteors.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        instanceMet = GameObject.FindWithTag("Meteor");
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        CheckLimit();
    }
}
