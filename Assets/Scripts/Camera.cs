﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public GameObject ship;
    public GameObject reference;
    private Vector3 distance;

    // Start is called before the first frame update
    void Start()
    {
        distance = transform.position - ship.transform.position;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (ship.GetComponent<Ship>().focus == Ship.Focus.Ship)
        {
            distance = Quaternion.AngleAxis(Input.GetAxis("Horizontal"), Vector3.up) * distance;

            transform.position = ship.transform.position + distance;
            transform.LookAt(ship.transform.position);

            reference.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            ship.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y -180, 0);
        }
    }
}
