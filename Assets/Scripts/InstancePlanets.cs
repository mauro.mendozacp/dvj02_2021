﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstancePlanets : MonoBehaviour
{
    public Planet planetPrefab; //Transform

    public List<Planet.PlanetData> planetsData;

    public List<Planet> generatedPlanets = new List<Planet>();

    public Material matAtmos;

    private String[] planetsName = {"Mercurio","Venus","Tierra","Marte","Jupiter","Saturno", "Urano","Neptuno"};

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < planetsData.Count; i++)
        {
            Planet.PlanetData pd = planetsData[i];

            GameObject goInst = Instantiate(planetPrefab).gameObject;

            Planet p = goInst.GetComponent<Planet>();
            p.Init(pd);
            p.name = planetsName[i];

            if (p.name == "Tierra")
            {
                GameObject atmos = Instantiate(planetPrefab).gameObject;
                atmos.name = "Atmosphere";

                Destroy(atmos.GetComponent<Planet>());
                Destroy(atmos.GetComponent<SphereCollider>());

                atmos.transform.parent = goInst.transform;  

                atmos.GetComponent<MeshRenderer>().material = matAtmos;

                atmos.transform.localScale = Vector3.one * 1.1f;
            }

            generatedPlanets.Add(p);

            goInst.transform.parent = transform;

            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
