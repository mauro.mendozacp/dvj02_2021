﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    [Serializable]
    public class PlanetData
    {
        public float traslationRadius;
        public float traslationSpeed;

        public Vector3 rotationAxis;
        public float rotationSpeed;

        public float size = 1;

        public Material mat;
    }
    
    public float speed = 2;
    public float angle = 0;
    public float radius = 2;

    public float rotationAngle = 0;
    public Vector3 wantedScale;
    public float rotationSpeed = 2;
    public Vector3 rotationDirection;

    public void Init(PlanetData pd)
    {
        radius = pd.traslationRadius;
        speed = pd.traslationSpeed;
        rotationDirection = pd.rotationAxis;
        rotationSpeed = pd.rotationSpeed;
        wantedScale = Vector3.one * pd.size;
        GetComponent<MeshRenderer>().material = pd.mat;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v3 = Vector3.zero;

        angle += speed * Time.deltaTime;

        v3.x = radius * Mathf.Cos(angle);
        v3.z = radius * Mathf.Sin(angle);

        transform.position = v3;

        transform.localScale = wantedScale;

        transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
    }
}
